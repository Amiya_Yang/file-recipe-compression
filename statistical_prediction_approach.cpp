#include <fstream>
#include <string>
using namespace std;

#define k 2

struct counter
{
	string s;
	int count;
	string prediction;	
};

void Misra_Gries(counter mp[k],const char *ifile_name)  
{
	ifstream input;
	input.open(ifile_name,ios::in);
	string trace,hash;
	bool flag = true,Isempty = false;
	getline(input,trace,'\n');  
	getline(input,trace,'\n');
	hash = trace.substr(0,17);                   
	mp[0].s = hash;                                        //读入第一个hash  
	for(int i=0;i<k;i++)
	{
		mp[i].count = 0;	
	}                
	while(getline(input,trace,'\n'))
	{
		hash = trace.substr(0,17);
		for(int i=0;i<k;i++)
		{
			if(hash == mp[i].s) 
			{
				mp[i].count++;
				flag = false;
				break;
			}
		}
		if(flag)
		{
			for(int i=0;i<k;i++)
			{
				if(mp[i].s == "0")
				{
					mp[i].s = hash;
					mp[i].count = 1;
					Isempty = true;
					break;
				}
			}
			if(!Isempty)
			{
				for(int i=0;i<k;i++)
				{
					mp[i].count--;
				}
				for(int i=0;i<k;i++)
				{
					if(mp[i].count == 0)
					{
						mp[i].s = "0";
						mp[i].count == 0;
					}	
				}
				for(int i=0;i<k;i++)
				{
					if(mp[i].s == "0")
					{
						mp[i].s = hash;
						mp[i].count = 1;
						break;	
					}	
				}	
			}
		}
	}
}

int main()
{
	const char *ifile_name = "fr_8kb.txt";
	const char *ofile_name = "statistical_prediction_comp_fr_8kb.txt";
	ifstream input;
	ofstream output;
	input.open(ifile_name,ios::in);
	output.open(ofile_name,ios::out|ios::trunc);
	counter mp[k];
	string trace,hash,com_trace;
	Misra_Gries(mp,ifile_name);
	getline(input,trace,'\n');
	bool HavePre = true,Pre = false;
	int context;
	output << trace << '\n';
	while(getline(input,trace,'\n'))
	{
		hash = trace.substr(0,17);
		if(!HavePre)
		{
			mp[context].prediction = hash;
			HavePre = true;	
		}
		if(Pre)
		{
			com_trace = trace.replace(0,17,"01");	
		}
		else com_trace = trace;
		output << com_trace << '\n';
		for(int i=0;i<k;i++)
		{
			if(hash == mp[i].s)  
			{
				context = i;
				Pre = true;
				if(mp[i].prediction == "0")
				{
					HavePre = false;	
				}
				break;
			}		
		}

	}
	return 0;
	
	
	
	
}