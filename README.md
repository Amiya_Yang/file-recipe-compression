This repository achieves four file recipe compression approaches from the paper "File Recipe Compression in Data Deduplication Systems".
"zero_chunk.cpp" detects zero chunk in the file recipe by detect the specific hash and replaces its hash with code "0"."page_oriented
_comp.cpp" assigns code word based on the page number and the least unique word in each page where chunk index stores to each chunk. 
"statistical_dictionary_approach.cpp" assign code word to the chunk whose information entropy is smaller than 85% of log2(n) where n
denotes the number of chunks."statistical_prediction_approach.cpp" uses Misra_Gries algorithm to approximate the detection of frequent
fingerprint pairs. I construct a struct to each frequent chunk that has a prediction of its next possible chunk.In the file recipe,if
a fingerprint prediction occurs correctly,then replace the whole finterprint with a "01" code.
