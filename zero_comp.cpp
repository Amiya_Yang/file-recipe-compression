#include <fstream>
#include <string>
#include <stdio.h>
using namespace std;

void CompFile(const char *ifile_name,const char *ofile_name)
{
	ifstream input;
	ofstream output;
	input.open(ifile_name,ios::in);
	output.open(ofile_name,ios::trunc);
	string trace,com_trace;
	while(getline(input,trace,'\n'))
	{
		if(trace.substr(0,17) == "b4:e2:20:5e:b6:00")
		{
			com_trace = trace.replace(0,17,"0");
			}
		else
		{
			com_trace = trace;
			}
		output << com_trace << '\n';
		}
	input.close();
	output.close();
	}
	
int main()
{
	const char *ifile_name = "fr_8kb.txt";
	const char *ofile_name = "zero_comp_fr_8kb.txt";
	CompFile(ifile_name,ofile_name);
	return 0;
	}
