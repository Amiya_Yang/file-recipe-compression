#include <fstream>
#include <string>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <iostream>
using namespace std;

struct Trace
{
	string index;
	int count ;
	double entropy ;
	char code[24];
};

int GetLineNum(const char *ifile_name)
{
	ifstream input;
	input.open(ifile_name,ios::in);
	string line;
	int line_num = 0;
	while(getline(input,line,'\n'))
	{
		line_num++;
	}
	return (line_num-1);
}

void CompFile(const char *ifile_name,const char *ofile_name)
{
	ifstream input;
	input.open(ifile_name,ios::in);
	ofstream output;
	output.open(ofile_name,ios::out|ios::trunc);
	int line_num,pointer = 0;
	line_num = GetLineNum(ifile_name);
	cout << line_num;
	string trace,hash,com_trace;
	Trace traces[line_num];
	for(int i=0;i<line_num;i++)
	{
		memset(traces[i].code,0,sizeof(traces[i].code));
		traces[i].count = 0;
		traces[i].entropy = 0;
	}
	bool FLAG = true;
	getline(input,trace,'\n');                     //读取文件头的说明文字 
	getline(input,trace,'\n');
	hash = trace.substr(0,17);                     //读入第一个chunk index 
	traces[pointer].index = hash;
	traces[pointer].count++;
	pointer++; 
	while(getline(input,trace,'\n'))
	{
		hash = trace.substr(0,17);
		for(int i=pointer-1;i>=0;i--)
		{
			if(hash == traces[i].index)  
			{
				FLAG = false;
				traces[i].count++;
				break;
			}
		}
		if(FLAG)
		{
			traces[pointer].index = hash;
			traces[pointer].count++;
			pointer++;
		}
		FLAG = true;	
	}
	for(int i=0;i<=pointer;i++)
	{
		traces[i].entropy = -log2(traces[i].count/line_num);
		if(traces[i].entropy < 0.85 * log2(pointer))
		{
			sprintf(traces[i].code,"%024u",i);
		}
	} 
	input.clear();
	input.seekg(0,ios::beg);
	getline(input,trace,'\n');
	output << trace << '\n';
	while(getline(input,trace,'\n'))
	{
		hash = trace.substr(0,17);
		com_trace = trace;
		for(int i=0;i<=pointer;i++)
		{
			if((hash == traces[i].index)&&(!strcmp(traces[i].code,"0")))
			{
				com_trace = trace.replace(0,17,traces[i].code);
			}
		}
		output << com_trace << '\n';
	}
	input.close();
	output.close(); 
}

int main()
{
	const char *ifile_name = "fr_8kb.txt";
	const char *ofile_name = "statistical0_comp_fr_8kb.txt";
	CompFile(ifile_name,ofile_name);
	return 0;
}
