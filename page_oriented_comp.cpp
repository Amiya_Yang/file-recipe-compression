#include <fstream>
#include <stdio.h>
#include <string>
#include <vector>
using namespace std;

#define page_size  21

void ExtLeastBitPerPage(ifstream &input,int &least_bits,int &line,vector<string> &str)
{
	int within_page_line = 0,uni_num = 1;
	bool FLAG = true;
	for(;within_page_line<page_size;within_page_line++)    
	{
		getline(input,str[within_page_line]);
		line++;
		}
	for(;uni_num<=17;uni_num++)
	{
		for(int a=1;a<page_size-1;a++)
		{
			if(FLAG)
			{
				for(int b=a+1;b<page_size;b++)
				{
					if(str[a].substr(0,uni_num) == str[b].substr(0,uni_num)) 
					{
						FLAG = false;
						break;
						}
					}
				}
			else break;
			}
		if(FLAG) break;
		}
	least_bits = uni_num;
	}
	
void CompFile(const char *ifile_name,const char *ofile_name)
{
	ifstream input;
	ofstream output;
	input.open(ifile_name,ios::in);
	output.open(ofile_name,ios::out|ios::trunc);
	string trace,comp_trace;
	vector<string> strings;
	getline(input,trace);
	int line = 2,within_page = 2,page;
	char page_s[20];
	while(!input.eof())
	{
		int least_bits = 1;
		ExtLeastBitPerPage(input,least_bits,line,strings);
		page = line / page_size;
		for(int a=0;a<page_size;a++)
		{
			comp_trace = strings[a].substr(0,least_bits) ;
			sprintf(page_s,"%d",page);
			comp_trace = comp_trace + page_s;
			output << comp_trace << '\n';
			}	
		}
	input.close();
	output.close();
	}

int main()
{
	const char *ifile_name = "fr_8kb.txt";
	const char *ofile_name = "page_oriented_comp_fr_8kb.txt";
	CompFile(ifile_name,ofile_name);
	return 0;
	}	
